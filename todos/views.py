from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoForm, TodoItemForm, EditForm

# Create your views here.


def todo_list(request):
    to_do_lists = TodoList.objects.all()
    context = {"to_do_lists": to_do_lists}
    return render(request, "todos_list/todo_list.html", context)


def todo_list_detail(request, id):
    tasks = get_object_or_404(TodoList, id=id)
    context = {
            "todo_object": tasks,
    }
    return render(request, "todos_list/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos_list/todo_list_create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=todo_list)

    context = {
        "form": form
    }

    return render(request, "todos_list/todo_list_update.html", context)


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos_list/todo_list_delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
        return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }

    return render(request, "todos_list/todo_item_create.html", context)


def todo_item_edit(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = EditForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            item.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = EditForm(instance=item)

    context = {
        "form": form
    }

    return render(request, "todos_list/todo_item_edit.html", context)
